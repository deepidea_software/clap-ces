package info.deepidea.ces.repository;

import info.deepidea.ces.domain.Answer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AnswersRepository extends PagingAndSortingRepository<Answer, Long> {
}
