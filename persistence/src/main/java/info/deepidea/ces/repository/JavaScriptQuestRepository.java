package info.deepidea.ces.repository;

import info.deepidea.ces.domain.JavaScriptQuest;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JavaScriptQuestRepository extends PagingAndSortingRepository<JavaScriptQuest, Long> {
}
