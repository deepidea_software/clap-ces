package info.deepidea.ces.quiz;


public enum QuestionType {
    OneAnswerQuestion,
    MultipleAnswersQuestion,
    OpenQuestion
}
