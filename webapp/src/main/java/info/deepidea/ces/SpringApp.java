package info.deepidea.ces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Andre on 5/24/2015.
 */
@ComponentScan
@EnableAutoConfiguration
public class SpringApp {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SpringApp.class);
        app.setShowBanner(false);
        app.run(args);
    }
}
