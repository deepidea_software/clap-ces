package info.deepidea.ces.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Andre on 5/24/2015.
 */
@Controller
@RequestMapping("/quest")
public class HomeController {
    @RequestMapping("/home")
    @ResponseBody
    public String home() {
        return "Clap Examination System v.1.0";
    }

    @RequestMapping("/say/{what}")
    @ResponseBody
    public String say(@PathVariable String what) {
        return what;
    }
}
