var cesModule = angular.module('cesModule', ['ngRoute', 'cesControllers']);

cesModule.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when("/list", {
            templateUrl: 'templates/list.html',
            controller: 'MessagesController'
        }).
        when('/quest', {
            templateUrl: 'templates/quest.html',
            controller: 'QuestsController'
        }).
        when('/uicomponents', {
            templateUrl: 'templates/uicomponents.html'
        }).
        otherwise({
            redirectTo: '/quest'
        });
}]);
